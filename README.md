# SBT Commands
*   sbt clean compile "testOnly HelloSpec" -> Execute multiple commands seq from terminal (is slower)
*   help <command> -> Displays detailed help for the specified command. 
### Test
*   ~testQuick -> Execute test until pass. Then press Enter
*   test -> Execute all test

*   reload -> reload sbt changes
*   projects -> List all projects

#### Projects [projectName]/[command]
*   compile -> Compile the project
*   run -> To run the app
*   project <projectname> -> to select a current project

### Docker
*   Docker/publishLocal -> Dockerize your app
*   doker run ref-arc:[version] -> run app in docker container (on terminal)
