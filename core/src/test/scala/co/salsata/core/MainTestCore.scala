package co.salsata.core

import org.scalatest.funsuite.AnyFunSuite

class MainTestCore extends AnyFunSuite {

  test("core:Check test core coreFun") {
    assert(Core.apply().coreFun().equals("coreFun"))
  }
}
