package co.salsata

import org.scalatest.funsuite.AnyFunSuite

class MainTest extends AnyFunSuite {

  test("ref-arc:Check test") {
    assert(functionRefArc.apply().check.equals(1))
  }

  test("ref-arc:integration:Check test") {
    assert(functionRefArc.apply().showCore().equals("coreFun"))
  }
}
