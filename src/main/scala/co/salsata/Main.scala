package co.salsata

import co.salsata.core.Core

object Main extends App {
  println("hello")
}

case class functionRefArc() {
  def check = 1
  private[this] def createCore() = Core.apply()
  def showCore() = createCore().coreFun()
}
