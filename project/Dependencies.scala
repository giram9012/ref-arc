import sbt._

object Dependencies {

  lazy val scalaTestVersion = "3.2.5"

  // Test
  val scalaTest = "org.scalatest" %% "scalatest" % scalaTestVersion

  // Core
  val coreDep = Seq(scalaTest % Test)
}
