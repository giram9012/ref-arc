
import Dependencies._

ThisBuild / scalaVersion := "2.13.5"
ThisBuild / version := "0.1"
ThisBuild / organization := "co.salsata"


lazy val refArc = (project in file("."))
  .aggregate(core)
  .dependsOn(core)
  .enablePlugins(JavaAppPackaging)
  .settings(
    name := "ref-arc",
    libraryDependencies ++= coreDep
   )

lazy val core = (project in file("core"))
  .settings(
    name := "Core",
    libraryDependencies ++= coreDep
  )



